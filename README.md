# Jerome - October 16th

## Installation
```
cd todomvc/
yarn (install dependencies)
yarn start
```

# Docker Compose
```
docker-compose up
[link](http://localhost:8081)
```

## Checklist

- [x] Can run app within Docker without installing Node.js on host
- [x] s3 code coverage bucket
- [x] github pages review page
- [ ] Review App for app
- [ ] Review App for test code coverage report

## Demo
<!-- Recommend testing these pages with Chrome Incognito to ensure we can view -->

**Merge Request:** [Merge Request](https://gitlab.com/romeski/gitlab-test/merge_requests/1)

**App Review App:** [gitlab pages link](http://romeski.gitlab.io/gitlab-test/)

**Coverage Review App:** [code coverage s3 link](http://code-coverage-347-788-8436.s3-website.us-east-1.amazonaws.com/)

## Features
 <!-- Any special features / interesting details with your implementation you want to explicitly note -->

## Security
# addressed
- AWS Key abstraction
- Bucket Policy

# not-addressed
- ENV abstractions

## Improvements
- cache reused docker images for more efficient pipeline runs
- bundle helper resources into custom build time Dockerfiles
- create custom runner cluster
- CI trigger from relevant files

---

## Other notes
- ran into issues with time management - started the project later than I would have liked -
<!-- Optional: Anything else you want to mention -->
