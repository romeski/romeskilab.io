FROM node:10
COPY /todomvc ./todomvc
RUN cd /todomvc && yarn
WORKDIR /todomvc
CMD ["yarn", "start"]
EXPOSE 3000